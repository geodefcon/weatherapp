//
//  MainVC.swift
//  WeatherApp
//
//  Created by user on 24/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var mainTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DataService.instance.loadTowns()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(update), name:"recivedDataFromServer", object: nil)
        
        mainTableView.dataSource = self
        mainTableView.delegate = self
        
        let weather = WeatherService()
        for town in DataService.instance.towns {
            weather.getWeather(town)
        }
    }
    
    func update() {
        dispatch_async(dispatch_get_main_queue(), {
            self.mainTableView.reloadData()
        })
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.weather.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let key = Array(DataService.instance.weather.keys)[indexPath.row]
        let dataDict = DataService.instance.weather[key]
     
        if let cell = tableView.dequeueReusableCellWithIdentifier("TownCell", forIndexPath: indexPath) as? TownCell {
            cell.configureCell(key, data: dataDict!)
            return cell
        } else {
            return TownCell()
        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("DetailVC", sender: indexPath.row)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetailVC" {
            if let detailsVC = segue.destinationViewController as? DetailVC {
                let key = Array(DataService.instance.weather.keys)[sender as! Int]
                detailsVC.key = key
            }
        }
    }

    
    @IBAction func AddTownPressed(sender: UIButton) {
        let alertController = UIAlertController(title: "Add Town", message: "", preferredStyle: .Alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .Default) { (action:UIAlertAction!) -> Void in
            let textField = alertController.textFields![0]
            guard let text = textField.text where text != "" else {
                return
            }
            DataService.instance.towns.append(text)
            DataService.instance.saveTowns()
            let weather = WeatherService()
            
            for town in DataService.instance.towns {
                weather.getWeather(town)
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default) { (action: UIAlertAction!) -> Void in
        }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            textField.placeholder = "Town name"
        }
        self.presentViewController(alertController, animated: true, completion:nil)
    }
}
