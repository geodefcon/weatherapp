//
//  DataService.swift
//  WeatherApp
//
//  Created by user on 24/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation
import UIKit

class DataService {
    static let instance = DataService()
    let KEY_TOWNS = "towns"
    let KEY_DAY_IN_FORECAST = "dayInForecast"
    var towns: [String] = [""]
    var weather = [String:[String:String]]()
    var forecast = [String:[String:String]]()
    var daysInForecast = 2

    
    func saveTowns() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(self.towns)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: KEY_TOWNS)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadTowns() {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(KEY_TOWNS) as? NSData {
            if let towns = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [String] {
                self.towns = towns
            }
        } else {
            self.towns = ["Москва", "Тула"]
        }
    }
    
    func saveDaysInForecast(days: Int) {
        let data = NSKeyedArchiver.archivedDataWithRootObject(days)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: KEY_DAY_IN_FORECAST)
        NSUserDefaults.standardUserDefaults().synchronize()
        self.daysInForecast = days
    }
    
    func loadDaysInForecast() {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(KEY_DAY_IN_FORECAST) as? NSData {
            if let days = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? Int {
                self.daysInForecast = days
            }
        } else {
            self.daysInForecast = 2
        }
    }
}