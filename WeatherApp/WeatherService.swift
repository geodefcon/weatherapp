//
//  WeatherService.swift
//  WeatherApp
//
//  Created by user on 24/10/16.
//  Copyright © 2016 user. All rights reserved.
//
import Foundation

class WeatherService {
    
    private let openWeatherMapBaseURL = "http://api.openweathermap.org/data/2.5/weather"
    private let openWeatherMapForecastURL = "http://api.openweathermap.org/data/2.5/forecast"
    private let openWeatherMapAPIKey = "b147af2c62a19e2f91ce184ea8bd9a93"
    let utils = Utils()
    
    func getWeather(city: String) {
        
        let session = NSURLSession.sharedSession()
        var urlString = "\(openWeatherMapBaseURL)?APPID=\(openWeatherMapAPIKey)&q=\(city)"
        
        urlString = urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let weatherRequestURL = NSURL(string: urlString)!
        
        let dataTask = session.dataTaskWithURL(weatherRequestURL) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let error = error {
                print("Error:\n\(error)")
            }
            else {
                do {
                   
                    let weather = try NSJSONSerialization.JSONObjectWithData(
                        data!,
                        options: .MutableContainers) as! [String: AnyObject]
 
                    var dict = [String:String]()

                    let city = weather["name"]! as? String
                    let temp = self.utils.kelvinToCelsius(weather["main"]!["temp"]!!)
                    
                    dict["temp"] = temp
                    dict["weather"] = weather["weather"]![0]!["description"]!! as? String
                    
                    dict["wind"] = String(weather["wind"]!["speed"]!!)
                    dict["pressure"] = String(weather["main"]!["pressure"]!!)
                    dict["humidity"] = String(weather["main"]!["humidity"]!!)
                    dict["cityID"] = String(weather["id"]!)
                    DataService.instance.weather[city!] = dict
                    NSNotificationCenter.defaultCenter().postNotificationName("recivedDataFromServer", object: nil)
                }
                catch let jsonError as NSError {
                    print("JSON error description: \(jsonError.description)")
                }
            }
        }
        dataTask.resume()
        
    }
    
    
    func getForecast(cityId: String) {
        
        let session = NSURLSession.sharedSession()
        let UrlString = "\(openWeatherMapForecastURL)?id=\(cityId)&appid=\(openWeatherMapAPIKey)"
        let urlString = UrlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
        let forecastRequestURL = NSURL(string: urlString)!
        
        let dataTask = session.dataTaskWithURL(forecastRequestURL) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) in
            if let error = error {
                print("Error:\n\(error)")
            }
            else {
                do {
                    let forecast = try NSJSONSerialization.JSONObjectWithData(
                        data!,
                        options: .MutableContainers) as! [String: AnyObject]
                    
                    let list = forecast["list"]! as? [[String:AnyObject]]
                    var temperatureDict = [String:[Float]]()
                    
                    for elem in list! {
                        let main = elem["main"] as? [String:AnyObject]
                        let date = elem["dt"] as? Double
                        let corrDate = NSDate(timeIntervalSince1970: date!)
                        let dateFormatter = NSDateFormatter()
                        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
                        let dateFromForecast = dateFormatter.stringFromDate(corrDate)
                        let todayDate = dateFormatter.stringFromDate(NSDate())
                        
                        guard dateFromForecast != todayDate else {
                            continue
                        }
                        
                        let temp = main!["temp"] as? Float
                        
                        if (temperatureDict.indexForKey(dateFromForecast) != nil) {
                            temperatureDict[dateFromForecast]?.append(temp!)
                        } else {
                            temperatureDict[dateFromForecast] = [temp!]
                        }
                    }
                    DataService.instance.forecast = [String:[String:String]]()
                    
                    for key in temperatureDict.keys {
                        let arr = temperatureDict[key]
                        let maxTemp = self.utils.kelvinToCelsius((arr?.maxElement())!)
                        let minTemp = self.utils.kelvinToCelsius((arr?.minElement())!)

                        let roundMax = String((Int(round(Float(maxTemp)!))))
                        let roundMin = String((Int(round(Float(minTemp)!))))
                        
                        DataService.instance.forecast[String(key)] = ["max":roundMax, "min":roundMin]
                    }
                    NSNotificationCenter.defaultCenter().postNotificationName("recivedForecast", object: nil)
                }
                catch let jsonError as NSError {
                    print("JSON error description: \(jsonError.description)")
                }
            }
        }
        dataTask.resume()
    }
}