//
//  Utils.swift
//  WeatherApp
//
//  Created by user on 25/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import Foundation

class Utils {
    
    func kelvinToCelsius(kelvin: AnyObject) -> String {
        let kelv = kelvin as! Float
        let celsius: Float = kelv - 273.15
        return String(celsius)
    }
}