//
//  TownCell.swift
//  WeatherApp
//
//  Created by user on 24/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class TownCell: UITableViewCell {
    @IBOutlet weak var cellTown: UILabel!
    @IBOutlet weak var cellTemp: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func configureCell(town: String, data: [String:String]) {
        cellTown.text = town
        let t = Float(data["temp"]!)
        cellTemp.text = String((Int(round(t!))))
    }
}