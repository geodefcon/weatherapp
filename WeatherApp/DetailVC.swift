//
//  DetailVC.swift
//  WeatherApp
//
//  Created by user on 25/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class DetailVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var forecastTableView: UITableView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var weather: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var daysInForecastBtn: UIButton!
    
    var key: String!
    var cityId: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(update), name:"recivedForecast", object: nil)
        let dataDict = DataService.instance.weather[key]
        
        forecastTableView.dataSource = self
        forecastTableView.delegate = self
        let forecast = WeatherService()
        
        city.text = key
        weather.text = dataDict!["weather"]!
        let t = Float(dataDict!["temp"]!)
        temperature.text = String((Int(round(t!))))
        wind.text = dataDict!["wind"]! + "m/s"
        pressure.text = dataDict!["pressure"]! + "hpa"
        humidity.text = dataDict!["humidity"]! + "%"
        self.cityId = dataDict!["cityID"]
        
        forecast.getForecast(cityId)
    }
    
    override func viewWillAppear(animated: Bool) {
        DataService.instance.loadDaysInForecast()
        let d = DataService.instance.daysInForecast
        if d == 2 {
            daysInForecastBtn.setTitle("forecast for 5 days", forState: .Normal)
        } else {
            daysInForecastBtn.setTitle("forecast for 2 days", forState: .Normal)
        }
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let keys = Array(DataService.instance.forecast.keys).sort()
        guard keys.count != 0 else { return UITableViewCell()}
        
        let key = keys[indexPath.row]
        let dataDict = DataService.instance.forecast[key]
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("ForecastCell", forIndexPath: indexPath) as? ForecastCell {
            cell.configureCell(key, data: dataDict!)
            return cell
        } else {
            return ForecastCell()
        }
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.daysInForecast
    }
    
    
    func update() {
        dispatch_async(dispatch_get_main_queue(), {
            self.forecastTableView.reloadData()
        })
    }
    
    @IBAction func leftSwipe(sender: UISwipeGestureRecognizer) {
        self.dismissViewControllerAnimated(true) {
        }
    }
    
    
    @IBAction func daysInForecastBtnPresd(sender: UIButton) {
        if DataService.instance.daysInForecast == 2 {
            DataService.instance.saveDaysInForecast(5)
            daysInForecastBtn.setTitle("forecast for 2 days", forState: .Normal)
        } else {
            DataService.instance.saveDaysInForecast(2)
            daysInForecastBtn.setTitle("forecast for 5 days", forState: .Normal)
        }
        update()
    }
}
