//
//  ForecastCell.swift
//  WeatherApp
//
//  Created by user on 25/10/16.
//  Copyright © 2016 user. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var minLbl: UILabel!
    @IBOutlet weak var maxLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(date: String, data: [String:String]) {
        dateLbl.text = date

        maxLbl.text = data["max"]
        minLbl.text = data["min"]
    }
}
